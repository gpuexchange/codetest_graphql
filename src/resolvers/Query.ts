import { getUserId, IContext } from "../utils";

export const Query = {
  feed(parent, args, ctx: IContext) {
    return ctx.prisma.posts({ where: { published: true } });
  },

  drafts(parent, args, ctx: IContext) {
    const id = getUserId(ctx);

    const where = {
      author: {
        id,
      },
      published: false,
    };

    return ctx.prisma.posts({ where });
  },

  post(parent, { id }, ctx: IContext) {
    return ctx.prisma.post({ id });
  },

  me(parent, args, ctx: IContext) {
    const id = getUserId(ctx);
    return ctx.prisma.user({ id });
  },
};
