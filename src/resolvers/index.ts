import { auth } from "./Mutation/auth";
import { post } from "./Mutation/post";
import { Post } from "./Post";
import { Query } from "./Query";
import { Subscription } from "./Subscription";
import { User } from "./User";

export default {
  Mutation: {
    ...auth,
    ...post,
  },
  Post,
  Query,
  Subscription,
  User,
};
